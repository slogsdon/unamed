<?php

namespace Unamed\Controllers {
    class Admin_Overview
    {
        protected $params = array();
        public function __construct(array $params = array())
        {
            $this->params = $params;
        }
    };
    class Admin_Posts
    {
        protected $params = array();
        public function __construct(array $params = array())
        {
            $this->params = $params;
        }
    };
    class Admin_Plugins
    {
        protected $params = array();
        public function __construct(array $params = array())
        {
            $this->params = $params;
        }
    };
    class Admin_Themes
    {
        protected $params = array();
        public function __construct(array $params = array())
        {
            $this->params = $params;
        }
    };
    class Admin_Settings
    {
        protected $params = array();
        public function __construct(array $params = array())
        {
            $this->params = $params;
        }
        public function edit()
        {
        }
    };
    class Admin_Users
    {
        protected $params = array();
        public function __construct(array $params = array())
        {
            $this->params = $params;
        }
    };
}
